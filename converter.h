#ifndef _CONVERTER_H_
#define _CONVERTER_H_

#include <iostream>
#include <string>
#include <vector>

#include "block.h"

class Converter
{
    public:
        Converter();

        std::string convert(std::vector<std::string> input);

    private:

        //Go through and simplify each of the blocks by combinings operators and operands
        void simplify();

        //Merge blocks with no remaining operators into previous block
        void merge();

        //Checks to see if a string is an operator
        bool isOperator(std::string str);

        //Checks to see if a string is an operand
        bool isOperand(std::string str);

        //Calculate the value of the resultant operand
        int calculateValue(std::string op, int a, int b);
        
        bool prevSymbolOperand = true;
        bool newBlock = true;
        std::vector<Block> blocks;
};

#endif