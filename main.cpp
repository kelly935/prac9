#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include "converter.h"

int main()
{
    // Get user input
    std::string input, tmp;
    getline(std::cin, input);

    std::vector<std::string> vec_in;

    std::stringstream stringstream(input);
    while(!stringstream.eof())
    {
        //Empty temp string
        tmp = "";
        stringstream >> tmp;

        vec_in.push_back(tmp);
    }

    Converter converter;

    //std::vector<std::string> input = {"*", "-", "5", "6", "7"};
    std::cout << converter.convert(vec_in) << std::endl;
}