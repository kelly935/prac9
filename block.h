#ifndef _BLOCK_H_
#define _BLOCK_H_

#include <vector>
#include <stack>
#include <string>
#include <iostream>

class Block
{
    public:
        typedef struct
        {
            std::string str = "";
            int val = 0;
        } Operand;

    private:
        std::stack<std::string> operators;
        std::vector<Operand> operands;

    public:

        //Checks to see if there are any operands left
        bool noOperandsLeft();

        //Print all information about the blocks current contents
        void printInfo();

        //Get the number of operators in the block
        int num_operators();

        //Get the number of operands in the block
        int num_operands();

        //Returns the next operator on the stack - does not remove it
        std::string peek_operator();

        //Returns the next operand in the queue - does not remove it
        Operand peek_operand();

        //Gets the next operator on the stack - this removes it from the block
        std::string get_operator();

        //Gets the next operand in the queue - this removes it from the block
        Operand get_operand();

        //Adds a new operator onto the stack
        void add_operator(std::string str);

        //Creates and adds a new operand to the queue by taking in a string and a value
        void add_operand_back(std::string str, int value);

        //Creates and adds a new operand to the queue, but puts it at the front of the queue
        void add_operand_front(std::string str, int value);

};

#endif