#include "block.h"

bool Block::noOperandsLeft()
{
    if(operands.size() == 0)
        return true;
    return false; 
}

void Block::printInfo()
{
    std::cout << "---- Block Info ----" << std::endl;
    int num_operators = operators.size();
    int num_operands = operands.size();
    std::stack<std::string> operators_copy = operators;
    std::vector<Operand> operands_copy = operands;

    for(int j = 0; j < num_operators; j++)
    {
        std::cout << "[Operator] " << operators_copy.top() << std::endl;
        operators_copy.pop();
    }

    for(int j = 0; j < num_operands; j++)
    {
        std::cout << "[Operand] Str: " << operands_copy.front().str << ", Val: " <<
        operands_copy.front().val <<  std::endl;
        operands_copy.erase(operands_copy.begin());
    }
    std::cout << "--------------------" << std::endl; 
}

int Block::num_operators()
{
    return operators.size();
}

int Block::num_operands()
{
    return operands.size();
}


std::string Block::peek_operator()
{
    if(!operators.empty())
    return operators.top();

    return "";
}

Block::Operand Block::peek_operand()
{
    if(!operands.empty())
    return operands.front();

    Operand op;
    return op;
}

std::string Block::get_operator()
{
    if(!operators.empty())
    {
        std::string result = operators.top();
        operators.pop();
        return result;
    }

    return "";
}

Block::Operand Block::get_operand()
{
    Operand result;
    if(!operands.empty())
    {
        result = operands.front();
        operands.erase(operands.begin());
        return result;
    }

    return result;
}

void Block::add_operator(std::string str)
{
    operators.push(str);
}

void Block::add_operand_front(std::string str, int value)
{
    Operand op;
    op.str = str;
    op.val = value;
    operands.insert(operands.begin(), op);
}

void Block::add_operand_back(std::string str, int value)
{
    Operand op;
    op.str = str;
    op.val = value;
    operands.push_back(op);
}