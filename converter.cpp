#include "converter.h"

Converter::Converter()
{

}

bool Converter::isOperator(std::string str)
{
    if(str == "+" || str == "-" || str == "*" || str == "/")
    {
        return true;
    }
    
    return false;
}

bool Converter::isOperand(std::string str)
{
    //Check to make sure that each char in str is a number
    for(unsigned int i = 0; i < str.length(); i++)
    {
        char c = str[i];
        if((int) c < 48 || (int) c > 57)
        {
            return false;
        }
    }

    return true;
}

int Converter::calculateValue(std::string op, int a, int b)
{
    if(op == "+")
    {
        return a + b;
    }
    if(op == "-")
    {
        return a - b;
    }
    //I am going to assume for now that we are not using floats, only ints
    if(op == "/")
    {
        return a / b;
    }
    if(op == "*")
    {
        return a * b;
    }
    else
    {
        std::cout << "Error: Unknown operator in calculateValue '" << op << "'" << std::endl;
        exit(1);
    }
}

void Converter::simplify()
{

    for(unsigned int i = 0; i < blocks.size(); i++)
    {

        //We need at least one operator to simplify
        if(blocks[i].num_operators() > 0)
        {
            //We also need two operands to simplify
            if(blocks[i].num_operands() >= 2)
            {
                //Get the next operator
                std::string op = blocks[i].get_operator();

                //Get the operands
                Block::Operand a = blocks[i].get_operand();
                Block::Operand b = blocks[i].get_operand();

                //Create new operand
                Block::Operand result;

                //Check when last equation, if so then do not use brackets
                if(blocks[i].num_operands() == 0)
                {
                    result.str = a.str + " " + op + " " +  b.str;
                }
                else
                {
                    result.str = "(" + a.str + " " + op + " " + b.str + ")";
                }

                result.val = calculateValue(op, a.val, b.val);

                //Put the simplified result back into the block
                blocks[i].add_operand_front(result.str, result.val);
            }
        }
    }
}

void Converter::merge()
{
    //Go through blocks backwards
    for(unsigned int i = 0; i < blocks.size(); i++)
    {
        int reverse_index = blocks.size() - (i + 1);

        if(blocks[reverse_index].num_operators() == 0)
        {
            //Extract remaining operands and put them in the next block (if we are at end block dont try to merge)
            if(i == blocks.size() - 1)
            {
                return;
            }

            int num_operands = blocks[reverse_index].num_operands();
            for(int j = 0; j < num_operands; j++)
            {
                Block::Operand op = blocks[reverse_index].get_operand();
                blocks[reverse_index - 1].add_operand_back(op.str, op.val);

            }
            
        }
    }

    //Clean up any empty blocks
    bool done = false;
    while(!done)
    {
        done = true; //We are done unless we find an empty block
        for(unsigned int i = 0; i < blocks.size(); i++)
        {
            if(blocks[i].num_operators() == 0 && blocks[i].num_operands() == 0)
            {
                //Delete the empty block
                blocks.erase(blocks.begin() + i);
                done = false;

                break; //Restart the for loop because deleting a block changes length of blocks
            }
        }
    }
}

std::string Converter::convert(std::vector<std::string> input)
{
    int block_num = -1;
    for(unsigned int i = 0; i < input.size(); i++)
    {
        //Get each character in the input
        std::string str = input[i];

        //Check if its an operator
        if(isOperator(str))
        {
            //If the previous symbol was an operand we create a new block
            if(prevSymbolOperand)
            {
                blocks.push_back(Block());
                newBlock = false;
                block_num++;
            }

            blocks.back().add_operator(str);
            prevSymbolOperand = false;
        }

        //Check if its an operand
        else if(isOperand(str))
        {
            blocks.back().add_operand_back(str, stoi(str));
            prevSymbolOperand = true;
        }

        //If we reach this point it is because there is an invalid character in the input string 
        else
        {
            std::cout << "Error" << std::endl;
            exit(1); // Exit the program
        }
    }

    //Need to check that operands and operators are balanced
    for(unsigned int i = 0; i < blocks.size(); i++)
    {
        // There should be exactly one less operator than there are opperands
        if(blocks[i].num_operands() - 1 != blocks[i].num_operators())
        {
            std::cout << "Error" << std::endl;
            exit(1);
        }
    }

    while(true)
    {

        if(blocks.size() == 1)
        {
            if(blocks[0].num_operators() == 0)
            {
                if(blocks[0].num_operands() == 1)
                {
                    Block::Operand op = blocks[0].get_operand();
                    return op.str + " = " + std::to_string(op.val);
                }
            }

        }

        simplify();
        merge();
    }
}

